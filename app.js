"use strict";
var express = require('express');
import logger       from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser   from 'body-parser';
import http         from 'http'

import routingPage       from './routes/routing';

var app = express();
const server = http.createServer(app);
let port = process.env.PORT || 3001;

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.set("view engine","twig");

// Ressources folders
app.use(express.static('views'));
app.use(express.static('ressources'));

// Routing call of the app
app.use('/', routingPage);

//The 404 Route 
app.get('*', function(req, res){
  res.render('home');
});

app.close = function() {
    server.close();
}

app.listen(() => {
    server.listen(port, () => {
        console.log("Express server listening on port " + port + " in " + app.settings.env + " mode");
    });
});

export default app;