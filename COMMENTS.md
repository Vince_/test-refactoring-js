1 - Recréer le routing 

- Création d'un page home
- Renommage des pages
- Changement de l'architecture des pages
- Refonte du routing
- Redirection sur la home en cas de 404

Ces changements on été réalisés pour faciliter l'expansion du site afin d'avoir une architecture propre et de pourvoir répartir le google juice de façon éfficace à l'avenir. 
Le libéllé des pages étaient trop global. 
Afin de ne pas perdre de google juice et de ne pas géner l'utilisateur toutes les erreures 404 redirigent sur la home.

2 - Aémliorer l'arborescence des dossiers et des fichiers

- Création d'un dossier ressources pour les images, fonts et css
- Création d'un dossier includes avec un fichier d'include pour le header, le menu et le footer
- Factorisation du code du routing

J'ai créé de nouveaux dossiers afin de rendre plus agréable l'architecture du site et des includes afin d'éviter le code redondant et facilité sa modification.

3 Amélioration du HTML/CSS

- Ajout d'un favicon
- Ajout de meta pour le SEO
- Ajout du code pour le partage sur les réseaux sociaux
- Ajout d'icônes fontawesome
- Ajout d'un menu responsive
- Ajout de google analytics pour le SEO
- Ajout d'un footer 
- Mise à jour du CSS et JS de bootstrap
- Amélioration ergonomique et du design de toutes les pages

4 Création de la page Cart

- Création de la page cart
- Renvoie du message de validation de l'achat dans la page HTML


Si plus de temps :

- Faire des animations pour rendre la navigation plus agréables
- Mieux travailler le SEO avec notamment des alt et du partage sur les réseaux sociaux
- Utiliser plus de balise spécifique comme strong et mettre plus de contenu pour le SEO (avec des mots clés)
- Créer une adresse de contact, rédigié les CGV et les mentions légales
- Améliorer le menu 
- Implémenter Less ou Sass et les fonctions gulp
- Améliorer le responsive
- Mettre en place le multilangue avec le fichier translate.json
- Créer une table en base de donnée pour accueillir les articles mis dans la panier et les ressortir à l'aide d'une requête SQL dans la page cart

(Suite à une erreur avec npm je n'ai pas la possibilité d'installer de nouveaux packages, j'aurais souhaité mettre gulp-less et gulp-csso.
J'ai tout de même laissé le code des fonctions que j'aurais utilisés dans le fichier gulpfile.js.)