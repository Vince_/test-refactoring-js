var express = require('express');
import { isAuth } from '../lib/middleware'

const router =  express.Router();

var sqlite3;
var db;

function database() {
    sqlite3 = require('sqlite3').verbose();
    db = new sqlite3.Database('database.sqlite');
}

/* Routing for home page*/

router.get('/', function(req, res) {
    res.render('home');
}) 

/* Routing for fragrance page */

router.get('/fragrance', function(req, res) {
    database();
    db.all("SELECT * FROM products", function(err, rows) {
        res.render('list', {products: rows});
    });

    db.close();
}) 

/* Routing for product page */

router.get('/fragrance/product/:id', isAuth, function(req, res) {
    var id = req.params.id;
    database();
    db.get("SELECT * FROM products WHERE id = " + id, function(err, row) {
        console.log(row);
        res.render('view', {product: row});
    });

    db.close();
});

/* Routing for cart page */

router.get('/cart/:id', isAuth, function(req, res) {
    var id = req.params.id;
    database();
    db.get("SELECT * FROM products WHERE id = " + id, function(err, row) {
        console.log(row);
        res.render('cart', {success:true, product: row});
    });

    db.close();
});


export default router;